﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication1.Models.DB
{
    public partial class Highschooler
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int? Grade { get; set; }
    }
}
