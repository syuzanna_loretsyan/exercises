﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication1.Models.DB
{
    public partial class BicycleInventory
    {
        public string MakeName { get; set; }
        public byte MakeId { get; set; }
        public string ModelName { get; set; }
        public byte ModelId { get; set; }
        public byte InStock { get; set; }
        public byte? OnOrder { get; set; }
    }
}
