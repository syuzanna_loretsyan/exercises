﻿using System;
using System.Collections.Generic;

#nullable disable

namespace WebApplication1.Models.DB
{
    public partial class Like
    {
        public int? Id1 { get; set; }
        public int? Id2 { get; set; }
    }
}
